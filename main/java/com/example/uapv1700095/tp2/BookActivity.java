package com.example.uapv1700095.tp2;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class BookActivity extends AppCompatActivity {
    BookDbHelper data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        BookDbHelper livre = new BookDbHelper(this);

        final EditText Nom = (findViewById(R.id.nameBook));
        final EditText Auteur = (findViewById(R.id.editAuthors));
        final EditText Annee = (findViewById(R.id.editYear));
        final EditText Genre = (findViewById(R.id.editGenres));
        final EditText Publier = (findViewById(R.id.editPublisher));
        final Button b = (Button) findViewById(R.id.boutton);


        //get id Book sélectionné dans l'item
        final Book book = getIntent().getExtras().getParcelable("book");

        if (book != null) {

            Nom.setText(book.getTitle(), TextView.BufferType.EDITABLE);
            Auteur.setText(book.getAuthors(), TextView.BufferType.EDITABLE);
            Annee.setText(book.getYear(), TextView.BufferType.EDITABLE);
            Genre.setText(book.getGenres(), TextView.BufferType.EDITABLE);
            Publier.setText(book.getPublisher(), TextView.BufferType.EDITABLE);
        }

        Button save = findViewById(R.id.button);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (book == null) {

                    Book book_ = new Book();
                    book_.setTitle(Nom.getText().toString());
                    book_.setAuthors(Auteur.getText().toString());
                    book_.setYear(Annee.getText().toString());
                    book_.setGenres(Genre.getText().toString());
                    book_.setPublisher(Publier.getText().toString());
                    if(Nom.getText().toString().equals("")){
                        AlertDialog alertDialog = new AlertDialog.Builder(BookActivity.this).create();
                        alertDialog.setTitle("Alert");
                        alertDialog.setMessage("Ajout Impossible ! ");
                        alertDialog.show();
                    }else{
                    data = new BookDbHelper(BookActivity.this);
                    data.addBook(book_);
                        Intent intent = new Intent(BookActivity.this, MainActivity.class);
                        finish();
                        startActivity(intent);}
                }
                else {
                    book.setTitle(Nom.getText().toString());
                    book.setAuthors(Auteur.getText().toString());
                           book.setGenres(Genre.getText().toString());
                    book.setPublisher(Publier.getText().toString());

                    BookDbHelper data = new BookDbHelper(BookActivity.this);
                    data.updateBook(book);
                    Intent intent = new Intent(BookActivity.this, MainActivity.class);
                    finish();
                    startActivity(intent);
                }
            }




    /*
    public void showMessage (String title, String message){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }
    */
});}}