package com.example.uapv1700095.tp2;


import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    BookDbHelper data;
    SimpleCursorAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        data = new BookDbHelper(getApplicationContext());

        // si nbr livres dans la liste == 0 alors on ajoute les livres dans la base de données
        if (data.fetchAllBooks().getCount() < 1) {
            data.populate();
        }


        Cursor result = data.fetchAllBooks();
        result.moveToFirst();
        final Cursor curseur = data.fetchAllBooks();
         adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, result, new String[]{BookDbHelper.COLUMN_BOOK_TITLE, BookDbHelper.COLUMN_AUTHORS}, new int[]{android.R.id.text1, android.R.id.text2}, 0);

        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        registerForContextMenu(listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor item = (Cursor) parent.getItemAtPosition(position);
                // Création de l'intent
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                // Ajout dans l'intent de l'objet livre
                intent.putExtra("book", data.cursorToBook(item));
                // Envoyer l'intent
                startActivity(intent);

            }
        });
        FloatingActionButton boutton = findViewById(R.id.boutton);
        boutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                intent.putExtra("book", "empty");
                startActivity(intent);
            }
        });

    }


   /* @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final Cursor cur = data.fetchAllBooks();
        cur.moveToPosition(info.position);
        int id = item.getItemId();
        if (id == R.id.action_settings) {
           data.deleteBook(cur);
            adapter.changeCursor(data.getReadableDatabase().query(data.TABLE_NAME, new String[] {"ROWID AS _id",data.COLUMN_BOOK_TITLE,data.COLUMN_AUTHORS},null, null, null, null, null));
            adapter.notifyDataSetChanged();
            return true;
        }

        return super.onContextItemSelected(item);
    }*/

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu,menu);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final Cursor cur = data.fetchAllBooks();
        cur.moveToPosition(info.position);
        int id = item.getItemId();
        if (id == R.id.supprimer) {
            //showMessage("Message","Suppression Succes");
            data.deleteBook(cur);
            adapter.changeCursor(data.getReadableDatabase().query(data.TABLE_NAME, new String[] {"ROWID AS _id",data.COLUMN_BOOK_TITLE,data.COLUMN_AUTHORS},null, null, null, null, null));
            adapter.notifyDataSetChanged();
            return true;
        }

        return super.onContextItemSelected(item);
    }

}